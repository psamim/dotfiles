set -g prefix M-m

unbind-key C-b

# Mouse Integration
set-option -g mouse-select-pane on
set-option -g mouse-select-window on
set-option -g mouse-resize-pane on
setw -g mode-mouse on

set-option -g default-shell /bin/zsh
set -g history-limit 100000
setw -g aggressive-resize on

bind-key r source-file ~/.tmux.conf
bind-key | split-window -h
bind-key - split-window -v

# utf8 support
set-window-option -g utf8 on

# mandatory for vim
set -s escape-time 1

# cycles thru windows
bind-key -n M-l next
bind-key -n M-h prev

# Create new window
bind-key -n M-n new-window

# Kill windows
bind-key -n M-q kill-pane

# copy-mode
bind-key -n M-c copy-mode

# Smart pane switching with awareness of vim splits
bind -n C-h run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-h) || tmux select-pane -L"
bind -n C-j run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-j) || tmux select-pane -D"
bind -n C-k run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-k) || tmux select-pane -U"
bind -n C-l run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-l) || tmux select-pane -R"
bind -n C-\ run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys 'C-\\') || tmux select-pane -l"

# Set the default terminal mode to 256color mode
set -g default-terminal "screen-256color"

# enable activity alerts
#setw -g monitor-activity on
set -g visual-activity on

bind -n M-t source-file ~/.tmux/vimo
bind -n M-a kill-pane -a


# vi-style controls for copy mode
setw -g mode-keys vi

# control automatic window renaming
setw -g automatic-rename on

# CPU/RAM/Load
set -g status-interval 2
# https://github.com/thewtex/tmux-mem-cpu-load
set -g status-right "#(tmux-mem-cpu-load 2 5)#[default]"

#### COLOUR (Solarized dark)

# default statusbar colors
set-option -g status-bg colour235 #base02
set-option -g status-fg colour130 #yellow
set-option -g status-attr default

# default window title colors
set-window-option -g window-status-fg colour33 #base0
set-window-option -g window-status-bg default
#set-window-option -g window-status-attr dim

# active window title colors
set-window-option -g window-status-current-fg colour196 #orange
set-window-option -g window-status-current-bg default
#set-window-option -g window-status-current-attr bright

# pane border
set-option -g pane-border-fg colour235 #base02
set-option -g pane-active-border-fg colour46 #base01

# message text
set-option -g message-bg colour235 #base02
set-option -g message-fg colour196 #orange

# pane number display
set-option -g display-panes-active-colour colour20 #blue
set-option -g display-panes-colour colour196 #orange

# clock
set-window-option -g clock-mode-colour colour40 #green

run-shell ~/.tmux/tmux-yank/yank.tmux

### END OF THEME
new-session
